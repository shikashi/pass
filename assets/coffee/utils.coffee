window.Utils =
  axisX: undefined
  axisY: undefined
  dragging: new Array
  containers: new Array

  dragable_onmousedown: ->
    Utils.dragging[@id] = true
    @style.left ||= "0px"
    @style.top  ||= "0px"

  dragable_onmouseup: ->
    Utils.dragging[@id] = false

  dragable_onmousemove: (event) ->
    x = Utils.axisX = event.clientX
    y = Utils.axisY = event.clientY
    if Utils.dragging[@id]
      @style.left = parseInt @style.left + x - Utils.axisX + "px"
      @style.top  = parseInt @style.top  + y - Utils.axisY + "px"

Container = (id, list, w, h) ->
  @id              = id
  @list            = list
  @movable         = false
  @el              = document.createElement "div"
  @el.id           = id
  @el.onclick      = Utils.borderBlink
  @el.className    = "container-maquette"
  @el.style.width  = if w then w + "px" else "auto"
  @el.style.height = if h then h + "px" else "auto"

  c1           = document.createElement "div"
  c1.innerHTML = "X"
  c1.className = "control-box"
  c1.onclick   = -> Utils.containers[id].destroy()
  @el.appendChild(c1)

  c1           = document.createElement "div"
  c1.innerHTML = "M"
  c1.className = "control-box"
  c1.onclick   = ->
    c = Utils.containers[id]
    if !c.movable
      @innerHTML = "F"
      c.setMovable true
    else
      @innerHTML = "M"
      c.setMovable false
  @el.appendChild c1

  c1           = document.createElement "div"
  c1.innerHTML = "i"
  c1.className = "control-box"
  c1.onclick   = -> alert "ID: " + id
  @el.appendChild c1
  @el.appendChild list.el
  document.getElementsByTagName("body")[0].appendChild @el

  @setMovable = (bool) ->
    if bool
      @movable         = true
      @el.onmouseup    = Utils.dragable_onmouseup
      @el.onmousemove  = Utils.dragable_onmousemove
      @el.onmousedown  = Utils.dragable_onmousedown
      @el.style.cursor = "pointer"
    else
      @movable = false
      @el.onmouseup    = @onMouseUp
      @el.onmousemove  = @onMouseMove
      @el.onmousedown  = @onMouseDown
      @el.style.cursor = "auto"

  @toString    = -> "[object Container]"
  @onMouseUp   = if typeof(@el.onmouseup)   == "function" then @el.onmouseup   else new Function("")
  @onMouseOut  = if typeof(@el.onmouseout)  == "function" then @el.onmouseout  else new Function("")
  @onMouseOver = if typeof(@el.onmouseover) == "function" then @el.onmouseover else new Function("")
  @onMouseDown = if typeof(@el.onmousedown) == "function" then @el.onmousedown else new Function("")
  @onMouseMove = if typeof(@el.onmousemove) == "function" then @el.onmousemove else new Function("")
  @destroy = ->
    @list.destroy()
    delete @list
    @el.parentNode.removeChild @el
    delete Utils.containers[@id]

List = (arr) ->
  @__list       = new Array()
  @el           = document.createElement "div"
  @el.className = "list-maquette"
  @toString = -> "[object List]"
  @populate(arr) if arr != undefined
  @populate = (arr) ->
    for i in arr
      c = new Node(this, i, arr[i])
      @appendChild c
  @appendChild = (node) ->
    @__list.push node
    @el.appendChild node.el
  @destroy = ->
    for i in @__list.length
      @__list[i].destroy()
      delete @__list[i]
    @el.parentNode.removeChild @el

walkUpTo = (el, nname, skip) ->
  p    = el.parentNode
  skip = 0 if skip == undefined

  if p.nodeName.toLowerCase() == nname.toLowerCase() && skip < 1
    return p
  else
    return walkUpTo p, nname, skip - 1

Node = (parent_list, nn, nv) ->
  @parent       = parent_list
  @expanded     = false
  @childList    = null
  @el           = document.createElement "div"
  @el.className = "node-maquette"

  tr    = document.createElement "tr"
  td_ex = document.createElement "td"
  td_nn = document.createElement "td"
  td_nv = document.createElement "td"
  table = document.createElement "table"

  inp       = document.createElement "input"
  inp.type  = "button"
  inp.value = "+"
  if (typeof(nv) != "object") || nv == null
    inp.disabled = true
  else
    __thisNode = this
    inp.onclick = (evnt) ->
      if __thisNode.expanded
        __thisNode.childList.destroy()
        __thisNode.childList = null
        __thisNode.expanded = false
        @value = "+"
      else
        if !evnt.shiftKey
          __thisNode.childList = new List(nv)
          __thisNode.childList.el.style.marginLeft = "10px"
          __thisNode.el.appendChild __thisNode.childList.el
          __thisNode.expanded = true
          @value = "-"
        else
          c_id = "id" + nn
          Utils.containers[c_id] = new Container(c_id, new List(nv), 750)

  td_ex.className = "ex"
  td_nn.className = "nn"
  td_nn.innerHTML = nn
  td_nv.innerHTML = nv
  td_ex.appendChild inp
  tr.appendChild td_ex
  tr.appendChild td_nn
  tr.appendChild td_nv
  table.appendChild tr
  @el.appendChild table

  @toString = -> "[object Node]"

  @destroy = -> @el.parentNode.removeChild @el

updateObjectType  = -> alert "not implemented"

ObjTypes = [
  {name: "Obj1", width: 1, height: 4}
  {name: "Obj2", width: 1, height: 3}
  {name: "Obj3", width: 1, height: 2}
  {name: "Obj4", width: 1, height: 2}
]

$T = (tag, collection) ->
  if collection == undefined
    document.getElementsByTagName tag
  else if collection instanceof HTMLCollection || collection instanceof Array
    elements = []
    for element in collection
      elements.pushEach (element.getElementsByTagName tag)
    elements.unique()
  else
    collection.getElementsByTagName tag

setup = ->
  i = 0
  sel = jQuery("#object_types")
  for object in ObjTypes
    el = document.createElement "option"
    el.value = i
    el.innerHTML = object.name
    sel.appendChild el
    i++