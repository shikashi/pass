class window.Map
  # Properties of the class
  # number of (1x1) boxes in the map
  number_of_boxes: 0
  # Homothetic scale map
  homothecy_ratio: 20
  # reference to the last (1x1) box inserted into the map
  last_created_box: undefined
  # Currently selected cell
  current_box: undefined

  # Given the number of rows and columns
  # create a new map (this will be a (wxh) box
  # containing smaller (1x1) boxes))
  create_map: (w, h) ->
    cols = parseInt w
    rows = parseInt h
    console.log "***(#{rows},#{cols})***"
    return if cols <= 0 || rows <= 0
    @remove_old_map()
    @number_of_boxes = 0
    canvas = jQuery('#canvas')[0]
    # Create the map, the box container of all other boxes
    @create_box canvas, cols, rows, '#666'
    for row in [1..rows]
      for col in [1..cols]
        # console.log "***(#{row},#{col})***"
        # Create a new (1x1) box and insert it into the map
        @create_box canvas, 1, 1, '#5588BB'
        # Properly positioning the new box on the map
        @move_box row, col
    window.onkeydown = @command_box

  remove_old_map: ->
    for i in [1..@number_of_boxes]
      console.log "#Box#{i}"
      jQuery("#Box#{i}").remove()

  create_box: (parent_element, width, height, colour) ->
    @number_of_boxes++
    # Crate a new node on the DOM for the new box,
    # and return the new HTML element
    box = @new_box width, height, colour
    # New box (cell) as a child of the map
    parent_element.appendChild box

  new_box: (width, height, colour) ->
    # Create a new div HTML element for the new cell
    box = document.createElement "div"
    # Set HTML attributes
    box.id = "Box" + @number_of_boxes
    box.title = "[id=#{box.id} width=#{width} height=#{height}]"
    box.style.top = "40px"
    box.style.left = "10px"
    box.style.width = "#{width * @homothecy_ratio}px"
    box.style.zIndex = @number_of_boxes + 1
    box.style.height = "#{height * @homothecy_ratio}px"
    box.style.position = "absolute"
    box.style.borderWidth = "1px"
    box.style.borderStyle = "solid"
    box.style.borderColor = "#808080"
    box.style.backgroundColor = colour
    # When clicked this is the currently selected box
    box.onclick = -> @current_box = this
    box.onmouseup = window.Utils.dragable_onmouseup
    box.onmousedown = window.Utils.dragable_onmousedown
    box.onmousemove = window.Utils.dragable_onmousemov
    # The last created box is the one being created now
    @last_created_box = box
    # console.log "***last created box: #{@last_created_box.id}***"
    box

  move_box: (top, left) ->
    box = if @current_box instanceof HTMLDivElement then @current_box else @last_created_box
    return if !(box instanceof HTMLDivElement)
    # Add some spacing between cells and
    # apply homothecy ratio
    box.style.top  = (top * @homothecy_ratio + 10) + "px"
    box.style.left = (left * @homothecy_ratio + 10) + "px"

  update_orientation: -> alert "not implemented"

  command_box: (evnt) ->
    console.log "***Started pattern matching on key pressed: #{evnt.keyCode}***"
    return if !(@current_box instanceof HTMLDivElement)
    switch evnt.keyCode
      # char r: rotate element
      when 82
        w = @current_box.style.width
        @current_box.style.width = @current_box.style.height
        @current_box.style.height = w
      # char delete: remove element
      when 46
        @current_box.parentNode.removeChild @current_box
        @current_box = null
      # char z: sink
      when 90
        z = parseInt @current_box.style.zIndex
        z = 0 if isNaN z
        @current_box.style.zIndex = if z == n + 2 then 0 else z + 1
      # char down-arrow: start moving
      when 40
        currentL = (parseInt(@current_box.style.left) - 10) / @homothecy_ratio
        currentT = (parseInt(@current_box.style.top)  - 10) / @homothecy_ratio
        switch evnt.keyCode
          # char left-arrow: move left 1 px
          when 37
            move_box currentL - 1, currentT
          # char up-arrow: move up 1 px
          when 38
            move_box currentL, currentT - 1
          # char right-arrow: move right 1 px
          when 39
            move_box currentL + 1, currentT
          # char down-arrow: move down 1 px
          when 40
            move_box currentL, currentT + 1
