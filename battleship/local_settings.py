# Local project root path.
PROJECT_ROOT = '/home/shikashi/devel/famaf/ing1/pass'

def pjrel(path):
    """
    Return a PROJECT_ROOT-relative path.
    """
    import os.path
    return os.path.join(PROJECT_ROOT, path)
