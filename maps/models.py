from django.db import models

class Map(models.Model):
    rows = models.IntegerField(default=9)
    columns = models.IntegerField(default=9)

    def __unicode__(self):
        return "Mapa => id: %(map_id)s, filas: %(map_rows)s, columnas: %(map_columns)s" % \
            {"map_id": self.id or "Null", "map_rows": self.rows, "map_columns": self.columns}

class Ship(models.Model):
    map = models.ForeignKey(Map)
    type = models.IntegerField()

    ship_types = {
        0: "Portaaviones",
        1: "Acorazado",
        2: "Fragata",
        3: "Submarino",
        4: "Bote de Patrulla",
    }

    def __unicode__(self):
        return "Barco => id: %(ship_id)s, mapa: %(ship_map)s, tipo: %(ship_type)s" % \
            {"ship_id": self.id or "Null", "ship_map": self.map.id or "Null", "ship_type": self.ship_types[self.type]}
